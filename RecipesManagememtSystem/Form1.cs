﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RecipesManagememtSystem
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            Recipe showMenu = new Recipe();
            showMenu.ShowDialog();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Ingredient showEdit = new Ingredient();
            showEdit.ShowDialog();
        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            Category categories = new Category();
            categories.ShowDialog();
        }
    }
}
