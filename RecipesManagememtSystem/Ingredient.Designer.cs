﻿namespace RecipesManagememtSystem
{
    partial class Ingredient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEdit = new System.Windows.Forms.DataGridView();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.lblrecipeName = new System.Windows.Forms.Label();
            this.lblCharge = new System.Windows.Forms.Label();
            this.lbxItems = new System.Windows.Forms.ListBox();
            this.lbxunits = new System.Windows.Forms.ListBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.comboquant = new System.Windows.Forms.ComboBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.comboName = new System.Windows.Forms.ComboBox();
            this.comboPerson = new System.Windows.Forms.ComboBox();
            this.lblamount = new System.Windows.Forms.Label();
            this.numAmount = new System.Windows.Forms.NumericUpDown();
            this.rtbShow = new System.Windows.Forms.RichTextBox();
            this.btnClear = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAmount)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEdit
            // 
            this.dgvEdit.AllowUserToAddRows = false;
            this.dgvEdit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvEdit.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.dgvEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEdit.Location = new System.Drawing.Point(804, 109);
            this.dgvEdit.Name = "dgvEdit";
            this.dgvEdit.ReadOnly = true;
            this.dgvEdit.RowHeadersVisible = false;
            this.dgvEdit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEdit.Size = new System.Drawing.Size(638, 354);
            this.dgvEdit.TabIndex = 0;
            this.dgvEdit.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvEdit_CellContentClick);
            // 
            // btnInsert
            // 
            this.btnInsert.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInsert.BackColor = System.Drawing.Color.Maroon;
            this.btnInsert.Font = new System.Drawing.Font("CommercialScript BT", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.ForeColor = System.Drawing.Color.White;
            this.btnInsert.Location = new System.Drawing.Point(28, 560);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(421, 80);
            this.btnInsert.TabIndex = 1;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = false;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdate.BackColor = System.Drawing.Color.Maroon;
            this.btnUpdate.Font = new System.Drawing.Font("CommercialScript BT", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(521, 560);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(421, 80);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnRemove.BackColor = System.Drawing.Color.Maroon;
            this.btnRemove.Font = new System.Drawing.Font("CommercialScript BT", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRemove.ForeColor = System.Drawing.Color.White;
            this.btnRemove.Location = new System.Drawing.Point(1018, 560);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(421, 80);
            this.btnRemove.TabIndex = 3;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // lblrecipeName
            // 
            this.lblrecipeName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblrecipeName.AutoSize = true;
            this.lblrecipeName.BackColor = System.Drawing.Color.Maroon;
            this.lblrecipeName.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrecipeName.ForeColor = System.Drawing.Color.White;
            this.lblrecipeName.Location = new System.Drawing.Point(14, 153);
            this.lblrecipeName.Name = "lblrecipeName";
            this.lblrecipeName.Size = new System.Drawing.Size(163, 21);
            this.lblrecipeName.TabIndex = 4;
            this.lblrecipeName.Text = "Recipe Name:";
            // 
            // lblCharge
            // 
            this.lblCharge.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblCharge.AutoSize = true;
            this.lblCharge.BackColor = System.Drawing.Color.Maroon;
            this.lblCharge.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCharge.ForeColor = System.Drawing.Color.White;
            this.lblCharge.Location = new System.Drawing.Point(389, 153);
            this.lblCharge.Name = "lblCharge";
            this.lblCharge.Size = new System.Drawing.Size(184, 21);
            this.lblCharge.TabIndex = 10;
            this.lblCharge.Text = "Category Name:";
            // 
            // lbxItems
            // 
            this.lbxItems.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbxItems.BackColor = System.Drawing.Color.PapayaWhip;
            this.lbxItems.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxItems.FormattingEnabled = true;
            this.lbxItems.ItemHeight = 19;
            this.lbxItems.Items.AddRange(new object[] {
            "Tomatoes",
            "Plantain",
            "Turnip",
            "Onion",
            "Lettuce",
            "Salt",
            "Pepper",
            "Herbs",
            "Beef",
            "Chicken",
            "Turkey",
            "Pork",
            "Crab",
            "Cheese",
            "Shrimp"});
            this.lbxItems.Location = new System.Drawing.Point(31, 211);
            this.lbxItems.Name = "lbxItems";
            this.lbxItems.Size = new System.Drawing.Size(208, 251);
            this.lbxItems.TabIndex = 15;
            // 
            // lbxunits
            // 
            this.lbxunits.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbxunits.BackColor = System.Drawing.Color.PapayaWhip;
            this.lbxunits.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxunits.FormattingEnabled = true;
            this.lbxunits.ItemHeight = 19;
            this.lbxunits.Items.AddRange(new object[] {
            "Teaspoon",
            "Cup",
            "Pint",
            "Quart",
            "Liter",
            "Gallon",
            "Gram"});
            this.lbxunits.Location = new System.Drawing.Point(245, 212);
            this.lbxunits.Name = "lbxunits";
            this.lbxunits.Size = new System.Drawing.Size(208, 251);
            this.lbxunits.TabIndex = 16;
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAdd.BackColor = System.Drawing.Color.Maroon;
            this.btnAdd.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(459, 288);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(121, 40);
            this.btnAdd.TabIndex = 18;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // comboquant
            // 
            this.comboquant.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboquant.BackColor = System.Drawing.Color.PapayaWhip;
            this.comboquant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboquant.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboquant.FormattingEnabled = true;
            this.comboquant.Items.AddRange(new object[] {
            "1/4",
            "1/3",
            "1/2",
            "2/3",
            "3/4",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboquant.Location = new System.Drawing.Point(459, 242);
            this.comboquant.Name = "comboquant";
            this.comboquant.Size = new System.Drawing.Size(121, 31);
            this.comboquant.TabIndex = 20;
            // 
            // lblTitle
            // 
            this.lblTitle.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTitle.BackColor = System.Drawing.Color.Maroon;
            this.lblTitle.Font = new System.Drawing.Font("CommercialScript BT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(18, 12);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(1424, 77);
            this.lblTitle.TabIndex = 21;
            this.lblTitle.Text = "Ingredients";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboName
            // 
            this.comboName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboName.BackColor = System.Drawing.Color.PapayaWhip;
            this.comboName.ForeColor = System.Drawing.Color.Black;
            this.comboName.FormattingEnabled = true;
            this.comboName.Location = new System.Drawing.Point(183, 156);
            this.comboName.Name = "comboName";
            this.comboName.Size = new System.Drawing.Size(180, 21);
            this.comboName.TabIndex = 22;
            // 
            // comboPerson
            // 
            this.comboPerson.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboPerson.BackColor = System.Drawing.Color.PapayaWhip;
            this.comboPerson.FormattingEnabled = true;
            this.comboPerson.Location = new System.Drawing.Point(601, 156);
            this.comboPerson.Name = "comboPerson";
            this.comboPerson.Size = new System.Drawing.Size(180, 21);
            this.comboPerson.TabIndex = 23;
            // 
            // lblamount
            // 
            this.lblamount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblamount.AutoSize = true;
            this.lblamount.BackColor = System.Drawing.Color.Maroon;
            this.lblamount.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblamount.ForeColor = System.Drawing.Color.White;
            this.lblamount.Location = new System.Drawing.Point(27, 490);
            this.lblamount.Name = "lblamount";
            this.lblamount.Size = new System.Drawing.Size(312, 21);
            this.lblamount.TabIndex = 25;
            this.lblamount.Text = "Amount in the WareHouse: ";
            // 
            // numAmount
            // 
            this.numAmount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numAmount.BackColor = System.Drawing.Color.PapayaWhip;
            this.numAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numAmount.Location = new System.Drawing.Point(345, 485);
            this.numAmount.Name = "numAmount";
            this.numAmount.ReadOnly = true;
            this.numAmount.Size = new System.Drawing.Size(397, 31);
            this.numAmount.TabIndex = 26;
            // 
            // rtbShow
            // 
            this.rtbShow.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtbShow.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtbShow.Location = new System.Drawing.Point(584, 208);
            this.rtbShow.Name = "rtbShow";
            this.rtbShow.Size = new System.Drawing.Size(212, 252);
            this.rtbShow.TabIndex = 27;
            this.rtbShow.Text = "";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnClear.BackColor = System.Drawing.Color.Maroon;
            this.btnClear.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(459, 334);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(121, 40);
            this.btnClear.TabIndex = 28;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // Ingredient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RecipesManagememtSystem.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1457, 654);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.rtbShow);
            this.Controls.Add(this.numAmount);
            this.Controls.Add(this.lblamount);
            this.Controls.Add(this.comboPerson);
            this.Controls.Add(this.comboName);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.comboquant);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbxunits);
            this.Controls.Add(this.lbxItems);
            this.Controls.Add(this.lblCharge);
            this.Controls.Add(this.lblrecipeName);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.dgvEdit);
            this.Name = "Ingredient";
            this.Text = "Edit";
            this.Load += new System.EventHandler(this.Ingredient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAmount)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEdit;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.Label lblrecipeName;
        private System.Windows.Forms.Label lblCharge;
        private System.Windows.Forms.ListBox lbxItems;
        private System.Windows.Forms.ListBox lbxunits;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox comboquant;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox comboName;
        private System.Windows.Forms.ComboBox comboPerson;
        private System.Windows.Forms.Label lblamount;
        private System.Windows.Forms.NumericUpDown numAmount;
        private System.Windows.Forms.RichTextBox rtbShow;
        private System.Windows.Forms.Button btnClear;
    }
}