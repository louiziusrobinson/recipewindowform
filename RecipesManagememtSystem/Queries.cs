﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace PicturesLibrary
{
    public class Queries
    {
        public static DataSet ExecuteQuery(string cmd)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=ZCM-797301620\SQLEXPRESS;Initial Catalog=manage_restaurant;Integrated Security=True");
            DataSet myDataSet = null;

            try
            {
                conn.Open();
                myDataSet = new DataSet();
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(cmd, conn);
                myDataAdapter.Fill(myDataSet);
                conn.Close();

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return myDataSet;
        }
    }
}
