﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace RecipesManagememtSystem
{
    public partial class Recipe : Form
    {
        SqlConnection conn = new SqlConnection(@"Data Source=ZCM-797301620\SQLEXPRESS;Initial Catalog=manage_restaurant;Integrated Security=True");
        OpenFileDialog browseImages = new OpenFileDialog();
        int recipeid = 0;
        string pictureName;
        string picPath;

        public Recipe()
        {
            InitializeComponent();
        }
        private void InitializeDialog()
        {

            browseImages.Filter = "Images|*.jpg;*.png;*.bmp";
            browseImages.Multiselect = true;
            browseImages.Title = "Image Browser";
        }
        private void LoadPicture(PictureBox picboxToLoad)
        {
            InitializeDialog();

            string saveFileFolder = @"Images\";

            if (browseImages.ShowDialog() == DialogResult.OK)
            {
                picboxToLoad.Image = Image.FromFile(browseImages.FileName);
                string fullPictureUrl = browseImages.FileName;
                int picIndex = fullPictureUrl.LastIndexOf(@"\");
                pictureName = (fullPictureUrl.Substring(picIndex)).Remove(0, 1);
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);
                picPath = fileFullPath + pictureName;

                
                if (!Directory.Exists(fileFullPath))
                {
                    Directory.CreateDirectory(fileFullPath);
                }
                picboxToLoad.Image.Save(picPath);
            }
            else
            {
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);
                picPath = fileFullPath + pictureName;
                picboxToLoad.Image.Save(picPath);
            }
        }
        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadPicture(pbxPic);

        }

        private void Recipe_Load(object sender, EventArgs e)
        {
            ShowAll();
        }
        private void ShowAll()
        {
            try
            {
                dgvMenu.DataSource = null;
                string queryStr = "SELECT * FROM recipes";
                DataSet ds = PicturesLibrary.Queries.ExecuteQuery(queryStr);
                dgvMenu.DataSource = ds.Tables[0];
                int lastDgvColumn = dgvMenu.ColumnCount;
                DataGridViewImageColumn imageColumn = new DataGridViewImageColumn();
                imageColumn.DataPropertyName = "Data";
                imageColumn.HeaderText = "recipe_image";
                imageColumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
                dgvMenu.Columns.Insert(lastDgvColumn, imageColumn);
                dgvMenu.RowTemplate.Height = 64;
                dgvMenu.Columns[lastDgvColumn].Width = 64;

                string saveFileFolder = @"Images\";
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);

                ds.Tables[0].Columns.Add("Data", Type.GetType("System.Byte[]"));

                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string picPath = fileFullPath + @"\" + row["recipe_image"].ToString();

                    row["Data"] = File.ReadAllBytes(picPath);
                }

                dgvMenu.DataSource = ds.Tables[0];
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error: " + ex.Message);
            
            }
        }
        
        private void dgvMenu_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            recipeid = Convert.ToInt16(dgvMenu.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtRecipeName.Text = dgvMenu.Rows[e.RowIndex].Cells[1].Value.ToString();
            rtxDescr.Text = dgvMenu.Rows[e.RowIndex].Cells[2].Value.ToString();
            combodiff.Text = dgvMenu.Rows[e.RowIndex].Cells[3].Value.ToString();
            mudMoney.Text = dgvMenu.Rows[e.RowIndex].Cells[4].Value.ToString();
            btnUpdate.Enabled = true;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to update this record?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (answer == DialogResult.Yes)
            {
                try
                {
                    string querStr = "UPDATE recipes SET recipe_name = '{0}'," +
                        " recipe_description = '{1}', " +
                        " recipe_difficulty = '{2}' , " +
                        " recipt_price = '{4}', " +
                        " recipe_image = '{3}', " +
                        " WHERE recipe_id = '{5}'";
                    conn.Open();
                    SqlCommand comm = conn.CreateCommand();
                    comm.CommandText = String.Format(querStr, txtRecipeName.Text, rtxDescr.Text, combodiff.Text, picPath, mudMoney.Text, recipeid);
                    comm.ExecuteNonQuery();
                    conn.Close();
                    ShowAll();
                    MessageBox.Show("Record successfully updated!");
                    txtRecipeName.Text = "";
                    rtxDescr.Text = "";
                    combodiff.Text = "Simple";
                    pbxPic.Image = null;
                    mudMoney.Text = "1";
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    conn.Close();
                    txtRecipeName.Text = "";
                    rtxDescr.Text = "";
                    combodiff.Text = "Simple";
                    pbxPic.Image = null;
                    mudMoney.Text = "";
                }
            }
        }

        private void btndele_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (answer == DialogResult.Yes)
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = conn.CreateCommand();
                    comm.CommandText = String.Format("DELETE FROM recipes WHERE recipe_id = '{0}'", recipeid);
                    comm.ExecuteNonQuery();
                    conn.Close();
                    ShowAll();
                    MessageBox.Show("Record successfully deleted!");
                    txtRecipeName.Text = "";
                    rtxDescr.Text = "";
                    combodiff.Text = "Simple";

                    pbxPic.Image = null;

                    mudMoney.Text = "";
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    conn.Close();
                    txtRecipeName.Text = "";
                    rtxDescr.Text = "";
                    combodiff.Text = "Simple";
                    pbxPic.Image = null;
                    mudMoney.Text = "";
                }
            }
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (txtRecipeName.Text != "" && rtxDescr.Text != "" && combodiff.Text != "" && pictureName != "" && mudMoney.Text != "" )
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        string getPicsCmd = $"INSERT INTO recipes VALUES ('{txtRecipeName.Text}', '{rtxDescr.Text}','{combodiff.Text}','{mudMoney.Value}','{pictureName}')";
                        DataSet ds = PicturesLibrary.Queries.ExecuteQuery(getPicsCmd);
                        MessageBox.Show("Record successfully inserted!");
                        btnUpdate.Enabled = true;
                        btndele.Enabled = true;
                        ShowAll();
                        txtRecipeName.Text = "";
                        rtxDescr.Text = "";
                        combodiff.Text = "Simple";
                        pbxPic.Image = null;
                        mudMoney.Text = "";

                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        conn.Close();
                        txtRecipeName.Text = "";
                        rtxDescr.Text = "";
                        combodiff.Text = "Simple";
                        pbxPic.Image = null;

                        mudMoney.Text = "";
                    }
                }
            }
            else
            {
                MessageBox.Show("please fill in every information about the Recipe");
            }
        }
    }
}
