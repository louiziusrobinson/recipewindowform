﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace RecipesManagememtSystem
{
    public partial class Ingredient : Form
    {
        public Ingredient()
        {
            InitializeComponent();
        }
        SqlConnection conn = new SqlConnection(@"Data Source=ZCM-797301620\SQLEXPRESS;Initial Catalog=manage_restaurant;Integrated Security=True");
        int ingid = 0;
        string show = "";
        private void ShowAll()
        {
            try
            {
                string queryStr = "SELECT ingredient_id AS [Ingredients ID], ingredient_units AS [Ingredient Units], ingredient_warehouse AS [WareHouse], recipe_name AS [Recipe Name], category_name [Category Name]" +
                    " FROM recipes, ingredients" +
                    " INNER JOIN categories ON ingredients.category_id = categories.category_id" +
                    " WHERE ingredients.recipe_id = recipes.recipe_id";
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = queryStr;
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                dgvEdit.DataSource = table;
                conn.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                conn.Close();
            }
        }
        private void Ingredient_Load(object sender, EventArgs e)
        {
            ShowAll();
            comboName.DataSource = GetAllRecipes();
            comboName.DisplayMember = "recipe_name";
            comboPerson.DataSource = GetAllCategories();
            comboPerson.DisplayMember = "category_name";
        }

        private void dgvEdit_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            ingid = Convert.ToInt16(dgvEdit.Rows[e.RowIndex].Cells[0].Value.ToString());
            rtbShow.Text = dgvEdit.Rows[e.RowIndex].Cells[1].Value.ToString();
            numAmount.Text = dgvEdit.Rows[e.RowIndex].Cells[2].Value.ToString();
            comboName.Text = dgvEdit.Rows[e.RowIndex].Cells[3].Value.ToString();
            comboPerson.Text = dgvEdit.Rows[e.RowIndex].Cells[4].Value.ToString();
            btnUpdate.Enabled = true;
        }

        private DataTable GetAllRecipes()
        {
            DataTable table = null;
            try
            {
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "SELECT * FROM recipes";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                conn.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                conn.Close();
            }
            return table;
        }
        private DataTable GetAllCategories()
        {
            DataTable table = null;
            try
            {
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "SELECT * FROM categories";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                conn.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                conn.Close();
            }
            return table;
        }

        private int GetRecipeid()
        {
            int recipeid = 0;
            try
            {
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = String.Format("SELECT recipe_id FROM recipes WHERE recipe_name = '{0}'", comboName.Text);
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                recipeid = Convert.ToInt16(table.Rows[0]["recipe_id"].ToString());
                conn.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                conn.Close();
            }
            return recipeid;
        }

        private int GetCategory()
        {
            int categoryid = 0;
            try
            {
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = String.Format("SELECT category_id FROM categories WHERE category_name = '{0}'", comboPerson.Text);
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                categoryid = Convert.ToInt16(table.Rows[0]["category_id"].ToString());
                conn.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                conn.Close();
            }
            return categoryid;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (lbxItems.SelectedItem != null && comboquant.SelectedItem != null && lbxunits.SelectedItem != null)
            {
                show += (comboquant.Text + " " + lbxunits.Text + " of " + lbxItems.Text +"\n");
                rtbShow.Text = show;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            show = "";
            rtbShow.Text = show;
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (comboName.Text != "" && comboPerson.Text != "" && rtbShow.Text != "")
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {
                        int recipeid = GetRecipeid();
                        int catid = GetCategory();
                        conn.Open();
                        SqlCommand comm = conn.CreateCommand();
                        comm.CommandText = String.Format("INSERT INTO ingredients VALUES ('{0}', '{1}', '{2}', '{3}')", rtbShow.Text, numAmount.Text, recipeid, catid);
                        comm.ExecuteNonQuery();
                        conn.Close();
                        ShowAll();
                        btnUpdate.Enabled = true;
                        btnRemove.Enabled = true;
                        rtbShow.Text = "";
                        show = "";
                        numAmount.Value = 0;
                        MessageBox.Show("Record successfully inserted!");

                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        conn.Close();
                        rtbShow.Text = "";
                        show = "";
                        numAmount.Value = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("please fill in every information about the Recipe");
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to update this record?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (answer == DialogResult.Yes)
            {
                try
                {
                    int selectednameid = GetRecipeid();
                    int selectedtitleid = GetCategory();
                    String queryStr = "UPDATE ingredients SET ingredient_units = '{0}', ingredient_warehouse = '{1}', recipe_id = '{2}' , category_id = '{3}' WHERE ingredient_id = '{4}'";
                    conn.Open();
                    SqlCommand comm = conn.CreateCommand();
                    comm.CommandText = String.Format(queryStr, rtbShow.Text, numAmount.Text, selectednameid, selectedtitleid, ingid);
                    comm.ExecuteNonQuery();
                    conn.Close();
                    ShowAll();
                    rtbShow.Text = "";
                    show = "";
                    numAmount.Value = 0;
                    MessageBox.Show("Record successfully updated!");
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    rtbShow.Text = "";
                    show = "";
                    numAmount.Value = 0;
                    conn.Close();
                }
            }

            
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (answer == DialogResult.Yes)
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = conn.CreateCommand();
                    comm.CommandText = String.Format("DELETE FROM ingredients WHERE ingredient_id = '{0}'", ingid);
                    comm.ExecuteNonQuery();
                    conn.Close();
                    ShowAll();
                    rtbShow.Text = "";
                    show = "";
                    numAmount.Value = 0;
                    MessageBox.Show("Record successfully deleted!");
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    rtbShow.Text = "";
                    show = "";
                    numAmount.Value = 0;
                    conn.Close();
                }
            }
        }
    }
}
