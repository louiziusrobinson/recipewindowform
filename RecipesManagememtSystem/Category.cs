﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;


namespace RecipesManagememtSystem
{
    public partial class Category : Form
    {

        public Category()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(@"Data Source=ZCM-797301620\SQLEXPRESS;Initial Catalog=manage_restaurant;Integrated Security=True");
        int categoryid = 0;

        private void ShowAll()
        {
            try
            {
                conn.Open();
                SqlCommand comm = conn.CreateCommand();
                comm.CommandText = "SELECT category_id as [Category ID], category_name as [Their Name], category_desciption as [Category Desciption], category_person as [Person in Charge] FROM categories";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                dvgCategory.DataSource = table;
                conn.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                conn.Close();
            }
        }
        private void dvgCategory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            categoryid = Convert.ToInt16(dvgCategory.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtname.Text = dvgCategory.Rows[e.RowIndex].Cells[1].Value.ToString();
            rtxDescr.Text = dvgCategory.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtperson.Text = dvgCategory.Rows[e.RowIndex].Cells[3].Value.ToString();
            btnUpdate.Enabled = true;
        }


        private void btnUpdate_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to update this record?", "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (answer == DialogResult.Yes)
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = conn.CreateCommand();
                    comm.CommandText = String.Format("UPDATE categories SET category_name = '{0}', category_desciption = '{1}', category_person = '{2}' WHERE category_id = '{3}'", txtname.Text, rtxDescr.Text, txtperson.Text, categoryid);
                    comm.ExecuteNonQuery();
                    conn.Close();
                    ShowAll();
                    MessageBox.Show("Record successfully updated!");
                    txtname.Text = "";
                    rtxDescr.Text = "";
                    txtperson.Text = "";
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    conn.Close();
                    txtname.Text = "";
                    rtxDescr.Text = "";
                    txtperson.Text = "";
                }

            }
        }
        private void btndelete_Click(object sender, EventArgs e)
        {
            DialogResult answer = MessageBox.Show("Are you sure you want to delete this record?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (answer == DialogResult.Yes)
            {
                try
                {
                    conn.Open();
                    SqlCommand comm = conn.CreateCommand();
                    comm.CommandText = String.Format("DELETE FROM categories WHERE category_id = '{0}'", categoryid);
                    comm.ExecuteNonQuery();
                    conn.Close();
                    ShowAll();
                    MessageBox.Show("Record successfully deleted!");
                    txtname.Text = "";
                    rtxDescr.Text = "";
                    txtperson.Text = "";
                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    conn.Close();
                    txtname.Text = "";
                    rtxDescr.Text = "";
                    txtperson.Text = "";
                }
            }
        }
        private void btnInsert_Click(object sender, EventArgs e)
        {
            if (txtname.Text != "" && rtxDescr.Text != "" && txtperson.Text != "" )
            {
                DialogResult answer = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    try
                    {

                        conn.Open();
                        SqlCommand comm = conn.CreateCommand();
                        comm.CommandText = String.Format("INSERT INTO categories VALUES ('{0}', '{1}', '{2}')", txtname.Text, rtxDescr.Text, txtperson.Text);
                        comm.ExecuteNonQuery();
                        conn.Close();
                        ShowAll();
                        btnUpdate.Enabled = true;
                        btndelete.Enabled = true;
                        txtname.Text = "";
                        rtxDescr.Text = "";
                        txtperson.Text = "";
                        MessageBox.Show("Record successfully inserted!");

                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        conn.Close();
                        txtname.Text = "";
                        rtxDescr.Text = "";
                        txtperson.Text = "";
                    }
                }
            }
            else
            {
                MessageBox.Show("please fill in every information about the Category");
            }
        }
        private void Category_Load(object sender, EventArgs e)
        {
            ShowAll();

        }


    }
}
