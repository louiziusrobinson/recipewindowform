﻿namespace RecipesManagememtSystem
{
    partial class Recipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMenu = new System.Windows.Forms.DataGridView();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtRecipeName = new System.Windows.Forms.TextBox();
            this.lblrecipeName = new System.Windows.Forms.Label();
            this.rtxDescr = new System.Windows.Forms.RichTextBox();
            this.lblDesc = new System.Windows.Forms.Label();
            this.combodiff = new System.Windows.Forms.ComboBox();
            this.lbldifficulty = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.lblPic = new System.Windows.Forms.Label();
            this.pbxPic = new System.Windows.Forms.PictureBox();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btndele = new System.Windows.Forms.Button();
            this.lblPrice = new System.Windows.Forms.Label();
            this.mudMoney = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mudMoney)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMenu
            // 
            this.dgvMenu.AllowUserToAddRows = false;
            this.dgvMenu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.dgvMenu.BackgroundColor = System.Drawing.Color.PapayaWhip;
            this.dgvMenu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMenu.Location = new System.Drawing.Point(15, 132);
            this.dgvMenu.Name = "dgvMenu";
            this.dgvMenu.ReadOnly = true;
            this.dgvMenu.RowHeadersVisible = false;
            this.dgvMenu.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMenu.Size = new System.Drawing.Size(563, 422);
            this.dgvMenu.TabIndex = 0;
            this.dgvMenu.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMenu_CellContentClick);
            // 
            // lblTitle
            // 
            this.lblTitle.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.lblTitle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTitle.BackColor = System.Drawing.Color.Maroon;
            this.lblTitle.Font = new System.Drawing.Font("CommercialScript BT", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(15, 29);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(948, 77);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Menu";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtRecipeName
            // 
            this.txtRecipeName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtRecipeName.BackColor = System.Drawing.Color.PapayaWhip;
            this.txtRecipeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRecipeName.Location = new System.Drawing.Point(760, 132);
            this.txtRecipeName.Name = "txtRecipeName";
            this.txtRecipeName.Size = new System.Drawing.Size(203, 29);
            this.txtRecipeName.TabIndex = 7;
            // 
            // lblrecipeName
            // 
            this.lblrecipeName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblrecipeName.AutoSize = true;
            this.lblrecipeName.BackColor = System.Drawing.Color.Maroon;
            this.lblrecipeName.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblrecipeName.ForeColor = System.Drawing.Color.White;
            this.lblrecipeName.Location = new System.Drawing.Point(587, 137);
            this.lblrecipeName.Name = "lblrecipeName";
            this.lblrecipeName.Size = new System.Drawing.Size(163, 21);
            this.lblrecipeName.TabIndex = 6;
            this.lblrecipeName.Text = "Recipe Name:";
            // 
            // rtxDescr
            // 
            this.rtxDescr.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.rtxDescr.BackColor = System.Drawing.Color.PapayaWhip;
            this.rtxDescr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtxDescr.Location = new System.Drawing.Point(760, 167);
            this.rtxDescr.Name = "rtxDescr";
            this.rtxDescr.Size = new System.Drawing.Size(203, 133);
            this.rtxDescr.TabIndex = 9;
            this.rtxDescr.Text = "";
            // 
            // lblDesc
            // 
            this.lblDesc.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDesc.AutoSize = true;
            this.lblDesc.BackColor = System.Drawing.Color.Maroon;
            this.lblDesc.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesc.ForeColor = System.Drawing.Color.White;
            this.lblDesc.Location = new System.Drawing.Point(616, 172);
            this.lblDesc.Name = "lblDesc";
            this.lblDesc.Size = new System.Drawing.Size(134, 21);
            this.lblDesc.TabIndex = 8;
            this.lblDesc.Text = "Desciption:";
            // 
            // combodiff
            // 
            this.combodiff.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.combodiff.BackColor = System.Drawing.Color.PapayaWhip;
            this.combodiff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combodiff.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combodiff.FormattingEnabled = true;
            this.combodiff.Items.AddRange(new object[] {
            "Simple",
            "Medium",
            "Complex"});
            this.combodiff.Location = new System.Drawing.Point(760, 306);
            this.combodiff.Name = "combodiff";
            this.combodiff.Size = new System.Drawing.Size(203, 32);
            this.combodiff.TabIndex = 11;
            // 
            // lbldifficulty
            // 
            this.lbldifficulty.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lbldifficulty.AutoSize = true;
            this.lbldifficulty.BackColor = System.Drawing.Color.Maroon;
            this.lbldifficulty.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldifficulty.ForeColor = System.Drawing.Color.White;
            this.lbldifficulty.Location = new System.Drawing.Point(629, 311);
            this.lbldifficulty.Name = "lbldifficulty";
            this.lbldifficulty.Size = new System.Drawing.Size(121, 21);
            this.lbldifficulty.TabIndex = 10;
            this.lbldifficulty.Text = "Difficulty:";
            // 
            // btnLoad
            // 
            this.btnLoad.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnLoad.BackColor = System.Drawing.Color.Maroon;
            this.btnLoad.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoad.ForeColor = System.Drawing.Color.White;
            this.btnLoad.Location = new System.Drawing.Point(651, 634);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(312, 32);
            this.btnLoad.TabIndex = 17;
            this.btnLoad.Text = "Load Picture";
            this.btnLoad.UseVisualStyleBackColor = false;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lblPic
            // 
            this.lblPic.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPic.AutoSize = true;
            this.lblPic.BackColor = System.Drawing.Color.Maroon;
            this.lblPic.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPic.ForeColor = System.Drawing.Color.White;
            this.lblPic.Location = new System.Drawing.Point(650, 408);
            this.lblPic.Name = "lblPic";
            this.lblPic.Size = new System.Drawing.Size(100, 21);
            this.lblPic.TabIndex = 16;
            this.lblPic.Text = "Picture:";
            // 
            // pbxPic
            // 
            this.pbxPic.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pbxPic.BackColor = System.Drawing.Color.PapayaWhip;
            this.pbxPic.Location = new System.Drawing.Point(651, 449);
            this.pbxPic.Name = "pbxPic";
            this.pbxPic.Size = new System.Drawing.Size(312, 179);
            this.pbxPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxPic.TabIndex = 15;
            this.pbxPic.TabStop = false;
            // 
            // btnInsert
            // 
            this.btnInsert.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnInsert.BackColor = System.Drawing.Color.Maroon;
            this.btnInsert.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInsert.ForeColor = System.Drawing.Color.White;
            this.btnInsert.Location = new System.Drawing.Point(34, 573);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(250, 71);
            this.btnInsert.TabIndex = 18;
            this.btnInsert.Text = "Insert";
            this.btnInsert.UseVisualStyleBackColor = false;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnUpdate.BackColor = System.Drawing.Color.Maroon;
            this.btnUpdate.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(310, 573);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(250, 71);
            this.btnUpdate.TabIndex = 19;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btndele
            // 
            this.btndele.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btndele.BackColor = System.Drawing.Color.Maroon;
            this.btndele.Font = new System.Drawing.Font("Broadway", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndele.ForeColor = System.Drawing.Color.White;
            this.btndele.Location = new System.Drawing.Point(34, 663);
            this.btndele.Name = "btndele";
            this.btndele.Size = new System.Drawing.Size(526, 71);
            this.btndele.TabIndex = 20;
            this.btndele.Text = "Delete";
            this.btndele.UseVisualStyleBackColor = false;
            this.btndele.Click += new System.EventHandler(this.btndele_Click);
            // 
            // lblPrice
            // 
            this.lblPrice.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Maroon;
            this.lblPrice.Font = new System.Drawing.Font("Broadway", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.White;
            this.lblPrice.Location = new System.Drawing.Point(674, 361);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(76, 21);
            this.lblPrice.TabIndex = 21;
            this.lblPrice.Text = "Price:";
            // 
            // mudMoney
            // 
            this.mudMoney.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mudMoney.BackColor = System.Drawing.Color.PapayaWhip;
            this.mudMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mudMoney.Location = new System.Drawing.Point(760, 357);
            this.mudMoney.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.mudMoney.Name = "mudMoney";
            this.mudMoney.Size = new System.Drawing.Size(203, 29);
            this.mudMoney.TabIndex = 24;
            this.mudMoney.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // Recipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RecipesManagememtSystem.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(979, 751);
            this.Controls.Add(this.mudMoney);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.btndele);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnInsert);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lblPic);
            this.Controls.Add(this.pbxPic);
            this.Controls.Add(this.combodiff);
            this.Controls.Add(this.lbldifficulty);
            this.Controls.Add(this.rtxDescr);
            this.Controls.Add(this.lblDesc);
            this.Controls.Add(this.txtRecipeName);
            this.Controls.Add(this.lblrecipeName);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.dgvMenu);
            this.Name = "Recipe";
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.Recipe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxPic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mudMoney)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMenu;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.TextBox txtRecipeName;
        private System.Windows.Forms.Label lblrecipeName;
        private System.Windows.Forms.RichTextBox rtxDescr;
        private System.Windows.Forms.Label lblDesc;
        private System.Windows.Forms.ComboBox combodiff;
        private System.Windows.Forms.Label lbldifficulty;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label lblPic;
        private System.Windows.Forms.PictureBox pbxPic;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btndele;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.NumericUpDown mudMoney;
    }
}